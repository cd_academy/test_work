//number // int float Number
//string String
//boolean
//null
//undefined

// Object
// Array


// function f() {
//
// }

// let x
// let x1, x2
// let x3 = 4, x4, x5 = x3 + 5

// const PI = 3.141592654,
//     E = 2.17

// console.log(PI)
// PI = 3

// + - * / // ++ -- += -= *= /= ** ()

// Math.round()

// '123' => +'123' => 123 => Number('123') => parseInt('123') => parseFloat('132')


// let x = 5

// console.log(!!x)

// console.log(Boolean(x))

// console.log(`${x + 2}`)
// console.log(String(x + 5))

// let y = String(x + 5)

// let y = x.toString()
// console.log(y);

// Math.cos(5)

// let x1 = new Object({qwd: 2})
// let x = {
//     a: 1,
//     b: {
//         z: 5
//     }
// }

// let y = {...x}
// let y = Object.assign({}, x)
// let y = x

// console.log(x, y)

// x.a = 9
// x.b.z = 7
//
// console.log(x, y)
//
// class A {
//
// }
//
// class B extends A {
//
// }
//
// new A()


// let x = 5
//
// let y = x
//
// x = 9
//
// console.log(y)


// let user = {
//
//     name: 'Test',
//     age: 45,
//     field_1: 5,
//     field_2: 55
//
// }

// console.log(user.name);

// let name = 'name'

// console.log(user[`field_${1+1}`])

// let a = [1, 2, 3, 5]
// let b = [...a]
// a[0] = 999
// console.log(a, b)
// console.log(a.length)

// console.log(a)

// Object.assign() // clone
// console.log(Object.keys({a: 1, b: 2})); // => ['a','b']
// console.log(Object.values({a: 1, b: 2}));
// console.log(Object.entries({a: 1, b: 2}));


//----------------------------------------------------------


// && || ! > < >= <=
// != ==

// console.log(0 == '0');
// console.log(0 === '0');

// !== ===


if (2 > 4 && 4 < 6) {
    console.log(2)
} else {
    console.log(1);
}

if (2 > 4 && 4 < 6) {
    console.log(2);
    let x;
    const t = 9;
}else if () {
    console.log(1);
} else if () {

} else
    let f


